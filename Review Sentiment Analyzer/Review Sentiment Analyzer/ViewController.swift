//
//  ViewController.swift
//  Review Sentiment Analyzer
//
//  Created by rifqi triginandri on 06/05/24.
//

import UIKit
import NaturalLanguage

class ViewController: UIViewController {
    
    @IBOutlet weak var sentimentImageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var clearButton: UIButton!
    
    private lazy var sentimentClassifier: NLModel? = {
        let model = try? NLModel(mlModel: ReviewClassifier().model)
        return model
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func onClearPressed(_ sender: Any) {
        textView.text = ""
        clearButton.isEnabled = false
    }
    
}

extension ViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.text.isEmpty == false,
            text == "\n" {
            //...
            if let label = sentimentClassifier?.predictedLabel(for: textView.text) {
                switch label {
                case "positive":                    
                    sentimentImageView.image = UIImage(named: "positive")
                case "negative":
                    sentimentImageView.image = UIImage(named: "negative")
                default:
                    sentimentImageView.image = UIImage(named: "what")
                }
            }
            
            textView.resignFirstResponder()
        }
        return true
    }

    func textViewDidChange(_ textView: UITextView) {
        clearButton.isEnabled = !textView.text.isEmpty
    }
}
